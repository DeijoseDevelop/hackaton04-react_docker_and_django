import {useState, useEffect} from 'react';
import {listGet} from '../../utils/listGet';
import PacienteItem from './PacienteItem';
import {deleteElement} from '../../utils/deleteElement';

export default function PacienteList(){

    const URL = 'paciente/';

    const [pacientes, setPacientes] = useState([])

    const getData = async () => {
        try{
            const data = await listGet(URL);
            setPacientes(data.results);
        } catch(error){
            console.log(error);
        }
    }
    useEffect(()=>{
        getData();
    }, []);
    return (
      <div className='concurrency paciente'>
        <div className='container text-center'>
          <h1 className='my-4'>Pacientes</h1>
          <table className='table table-dark table-striped'>
            <thead>
              <tr>
                <th scope='col'>nombres</th>
                <th scope='col'>apellidos</th>
                <th scope='col'>dni</th>
                <th scope='col'>direccion</th>
                <th scope='col'>telefono</th>
                <th scope='col'>sexo</th>
                <th scope='col'>fecha_nacimiento</th>
                <th scope='col'>update</th>
                <th scope='col'>delete</th>
              </tr>
            </thead>
            <tbody>
              {pacientes
                ? pacientes.map((paciente) => {
                    return (
                      <PacienteItem
                        key={paciente.id}
                        paciente={paciente}
                        deletePaciente={deleteElement}
                      />
                    );
                  })
                : null}
            </tbody>
          </table>
        </div>
      </div>
    );
}