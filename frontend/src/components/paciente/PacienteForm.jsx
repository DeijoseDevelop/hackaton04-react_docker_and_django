import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { addPaciente } from "../../utils/paciente/addPaciente";
import { updatePaciente } from "../../utils/paciente/updatePaciente";
import { getElement } from "../../utils/getElement";

export default function EspecialidadFormPacienteForm() {
  const params = useParams();
  const navigate = useNavigate();
  const initialState = {
    nombres: '',
    apellidos: '',
    dni: '',
    direccion: '',
    telefono: '',
    sexo: '',
    fecha_nacimiento: '',
  };
  const [paciente, setPaciente] = useState(initialState);

  const handleInputChange = (e) => {
    setPaciente({
      ...paciente,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res;
      if (!params.id) {
        res = await addPaciente(paciente);
        if (res.status === 201) {
          setPaciente(initialState);
        }
      } else {
        updatePaciente(params.id, paciente);
      }
      navigate('/listPaciente');
    } catch (error) {
      console.log(error);
    }
  };
  const getPaciente = async (id) => {
    try {
      const paciente = await getElement(params.id, 'paciente/');
      setPaciente({ ...paciente.data });
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    if (params.id) {
      getPaciente(params.id);
    }
  }, [params.id]);

  return (
    <div className='concurrency paciente'>
      <div className='container col-xl-4 col-md-6'>
        <h1 className='text-center my-4'>
          {params.id ? 'Update Paciente' : 'Add Paciente'}
        </h1>
        <form onSubmit={handleSubmit}>
          <div className='form-group'>
            <label>Nombres</label>
            <input
              type='text'
              name='nombres'
              className='form-control'
              value={paciente.nombres}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Apellidos</label>
            <input
              type='text'
              name='apellidos'
              className='form-control'
              value={paciente.apellidos}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>DNI</label>
            <input
              type='text'
              name='dni'
              className='form-control'
              value={paciente.dni}
              onChange={handleInputChange}
              maxLength='8'
            />
          </div>
          <div className='form-group'>
            <label>Dirección</label>
            <input
              type='text'
              name='direccion'
              className='form-control'
              value={paciente.direccion}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Telefono</label>
            <input
              type='text'
              name='telefono'
              className='form-control'
              value={paciente.telefono}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-check'>
            <input
              className='form-check-input'
              type='radio'
              name='sexo'
              id='flexRadioDefault1'
              value='M'
              onChange={handleInputChange}
            />
            <label className='form-check-label' htmlFor='flexRadioDefault1'>
              Masculino
            </label>
          </div>
          <div className='form-check'>
            <input
              className='form-check-input'
              type='radio'
              name='sexo'
              id='flexRadioDefault2'
              value='F'
              onChange={handleInputChange}
            />
            <label className='form-check-label' htmlFor='flexRadioDefault2'>
              Femenino
            </label>
          </div>
          <div className='form-group'>
            <label>Fecha Nacimiento</label>
            <input
              type='date'
              name='fecha_nacimiento'
              className='form-control'
              value={paciente.fecha_nacimiento}
              onChange={handleInputChange}
            />
          </div>
          {params.id ? (
            <button type='submit' className='btn btn-block btn-primary mt-3'>
              Update
            </button>
          ) : (
            <button type='submit' className='btn btn-block btn-success mt-3'>
              Register
            </button>
          )}
        </form>
      </div>
    </div>
  );
}