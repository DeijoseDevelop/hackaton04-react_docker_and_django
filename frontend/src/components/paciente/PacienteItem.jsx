import { useNavigate } from 'react-router-dom';

export default function PacienteItem({ paciente, deletePaciente }) {
  const endpoint = 'paciente/';
  const navigate = useNavigate();
  return (
    <tr>
      <td>{paciente.nombres}</td>
      <td>{paciente.apellidos}</td>
      <td>{paciente.dni}</td>
      <td>{paciente.direccion}</td>
      <td>{paciente.telefono}</td>
      <td>{paciente.sexo}</td>
      <td>{paciente.fecha_nacimiento}</td>
      <td>
        <button
          onClick={() => navigate(`updatePaciente/${paciente.id}`)}
          className='btn btn-primary'
        >
          update
        </button>
      </td>
      <td>
        <button
          onClick={() => deletePaciente(paciente.id, endpoint)}
          className='btn btn-danger'
        >
          delete
        </button>
      </td>
    </tr>
  );
}
