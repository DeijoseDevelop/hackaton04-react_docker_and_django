import { useState, useEffect } from 'react';
import { listGet } from '../../utils/listGet';
import HorarioItem from './HorarioItem';
import {deleteElement} from '../../utils/deleteElement';

export default function HorarioList() {
  const URL = 'horario/';

  const [horarios, setHorarios] = useState([]);

  const getData = async () => {
    try {
      const data = await listGet(URL);
      setHorarios(data.results);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className='concurrency horarios'>
      <div className='col-md-12 col-lg-10 mx-auto text-center'>
        <h1 className='my-4'>Horarios</h1>
        <table className='table table-dark table-striped'>
          <thead>
            <tr>
              <th scope='col'>medico_id</th>
              <th scope='col'>activo</th>
              <th scope='col'>fecha_registro</th>
              <th scope='col'>usuario_registro</th>
              <th scope='col'>fecha_modificacion</th>
              <th scope='col'>usuario_modificacion</th>
              <th scope='col'>fecha_atencion</th>
              <th scope='col'>inicio_atencion</th>
              <th scope='col'>fin_atencion</th>
              <th scope='col'>update</th>
              <th scope='col'>delete</th>
            </tr>
          </thead>
          <tbody>
            {horarios
              ? horarios.map((horario) => {
                  return (
                    <HorarioItem
                      key={horario.id}
                      horario={horario}
                      deleteHorario={deleteElement}
                    />
                  );
                })
              : null}
          </tbody>
        </table>
      </div>
    </div>
  );
}
