import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { addHorario } from '../../utils/horario/addHorario';
import { updateHorario } from '../../utils/horario/updateHorario';
import { getElement } from '../../utils/getElement';
import { listGet } from '../../utils/listGet';

export default function HorarioForm() {
  const params = useParams();
  const navigate = useNavigate();
  const initialState = {
    medico_id: '',
    activo: false,
    fecha_registro: '',
    usuario_registro: '',
    fecha_modificacion: '',
    usuario_modificacion: '',
    fecha_atencion: '',
    inicio_atencion: '',
    fin_atencion: '',
  };
  const [horario, setHorario] = useState(initialState);
  const [medicos, setMedicos] = useState([]);

  const handleInputChange = (e) => {
    setHorario({
      ...horario,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res;
      if (!params.id) {
        res = await addHorario(horario);
        if (res.status === 201) {
          setHorario(initialState);
        }
      } else {
        updateHorario(params.id, horario);
      }
      navigate('/listHorario');
    } catch (error) {
      console.log(error);
    }
  };
  const getHorario = async (id) => {
    try {
      const horario = await getElement(id, 'horario/');
      setHorario({ ...horario.data });
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    listGet('medico/').then((data) => setMedicos(data.results));
    if (params.id) {
      getHorario(params.id);
    }
  }, [params.id]);
  return (
    <div className='concurrency horarios'>
      <div className='container col-xl-4 col-md-6'>
        <h1 className='text-center my-4'>
          {params.id ? 'Update Horario' : 'Add Horario'}
        </h1>
        <form onSubmit={handleSubmit}>
          <select
            className='form-select form-select-sm mb-3'
            aria-label='.form-select-lg example'
            name='medico_id'
            onChange={handleInputChange}
            value={horario.medico_id}
          >
            <option selected>Open this select menu</option>
            {medicos
              ? medicos.map((medico) => {
                  return (
                    <option key={medico.id} value={medico.id}>
                      {`${medico.nombre} ${medico.apellido}`}
                    </option>
                  );
                })
              : null}
          </select>
          <div className='form-check'>
            <input
              className='form-check-input'
              type='radio'
              name='activo'
              id='flexRadioDefault1'
              value={true}
              onChange={handleInputChange}
            />
            <label className='form-check-label' htmlFor='flexRadioDefault1'>
              Activo
            </label>
          </div>
          <div className='form-check'>
            <input
              className='form-check-input'
              type='radio'
              name='activo'
              id='flexRadioDefault2'
              value={false}
              onChange={handleInputChange}
            />
            <label className='form-check-label' htmlFor='flexRadioDefault2'>
              Inactivo
            </label>
          </div>
          <div className='form-group'>
            <label>Fecha Registro</label>
            <input
              type='date'
              name='fecha_registro'
              className='form-control'
              value={horario.fecha_registro}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Usuario Registro</label>
            <input
              type='date'
              name='usuario_registro'
              className='form-control'
              value={horario.usuario_registro}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Fecha Modificacion</label>
            <input
              type='date'
              name='fecha_modificacion'
              className='form-control'
              value={horario.fecha_modificacion}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Usuario Modificacion</label>
            <input
              type='date'
              name='usuario_modificacion'
              className='form-control'
              value={horario.usuario_modificacion}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Fecha Atencion</label>
            <input
              type='date'
              name='fecha_atencion'
              className='form-control'
              value={horario.fecha_atencion}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Inicio Atencion</label>
            <input
              type='time'
              name='inicio_atencion'
              className='form-control'
              value={horario.inicio_atencion}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Fin Atencion</label>
            <input
              type='time'
              name='fin_atencion'
              className='form-control'
              value={horario.fin_atencion}
              onChange={handleInputChange}
            />
          </div>
          {params.id ? (
            <button type='submit' className='btn btn-block btn-primary mt-3'>
              Update
            </button>
          ) : (
            <button type='submit' className='btn btn-block btn-success mt-3'>
              Register
            </button>
          )}
        </form>
      </div>
    </div>
  );
}
