import {useNavigate} from 'react-router-dom';

export default function HorarioItem({ horario, deleteHorario }) {
  const endpoint = 'horario/';
  const navigate = useNavigate();
  return (
    <tr>
      <td>{horario.medico_id}</td>
      <td>
        {horario.activo ? (
          <span className='badge bg-success'>True</span>
        ) : (
          <span className='badge bg-danger'>False</span>
        )}
      </td>
      <td>{horario.fecha_registro}</td>
      <td>{horario.usuario_registro}</td>
      <td>{horario.fecha_modificacion}</td>
      <td>{horario.usuario_modificacion}</td>
      <td>{horario.fecha_atencion}</td>
      <td>{horario.inicio_atencion}</td>
      <td>{horario.fin_atencion}</td>
      <td>
        <button
          onClick={() => navigate(`updateHorario/${horario.id}`)}
          className='btn btn-primary'
        >
          update
        </button>
      </td>
      <td>
        <button
          onClick={() => deleteHorario(horario.id, endpoint)}
          className='btn btn-danger'
        >
          delete
        </button>
      </td>
    </tr>
  );
}