import { useState, useEffect } from 'react';
import { listGet } from '../../utils/listGet';
import MedicoEspecialidadItem from './MedicoEspecialidadItem';
import {deleteElement} from '../../utils/deleteElement';

export default function MedicoEspecialidadList() {
  const URL = 'medicoEspecialidad/';

  const [medicoEspecialidades, setMedicoEspecialidades] = useState([]);

  const getData = async () => {
    try {
      const data = await listGet(URL);
      setMedicoEspecialidades(data.results);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className='concurrency especialidades'>
      <div className='container text-center'>
        <h1 className='my-4'>MedicoEspecialidades</h1>
        <table className='table table-dark table-striped'>
          <thead>
            <tr>
              <th scope='col'>medico_id</th>
              <th scope='col'>especialidad_id</th>
              <th scope='col'>update</th>
              <th scope='col'>delete</th>
            </tr>
          </thead>
          <tbody>
            {medicoEspecialidades
              ? medicoEspecialidades.map((medicoEspecialidad) => {
                  return (
                    <MedicoEspecialidadItem
                      key={medicoEspecialidad.id}
                      medicoEspecialidad={medicoEspecialidad}
                      deleteMedicoEspecialidad={deleteElement}
                    />
                  );
                })
              : null}
          </tbody>
        </table>
      </div>
    </div>
  );
}
