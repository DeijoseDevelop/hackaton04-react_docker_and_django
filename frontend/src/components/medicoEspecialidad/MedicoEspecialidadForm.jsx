import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { addMedicoEspecialidad } from '../../utils/medicoEspecialidad/addMedicoEspecialidad';
import { updateMedicoEspecialidad } from '../../utils/medicoEspecialidad/updateMedicoEspecialidad';
import { getElement } from '../../utils/getElement';
import { listGet } from '../../utils/listGet';

export default function MedicoEspecialidadForm() {
  const params = useParams();
  const navigate = useNavigate();
  const initialState = {
    medico_id: '',
    especialidad: '',
  };
  const [medicoEspecialidad, setMedicoEspecialidad] = useState(initialState);
  const [medicos, setMedicos] = useState([]);
  const [especialidades, setEspecialidades] = useState([]);

  const handleInputChange = (e) => {
    setMedicoEspecialidad({
      ...medicoEspecialidad,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res;
      if (!params.id) {
        console.log(medicoEspecialidad);
        res = await addMedicoEspecialidad(medicoEspecialidad);
        if (res.status === 201) {
          setMedicoEspecialidad(initialState);
        }
      } else {
        updateMedicoEspecialidad(params.id, medicoEspecialidad);
      }
      navigate('/listMedicosEspecialidades');
    } catch (error) {
      console.log(error);
    }
  };
  const getMedicoEspecialidad = async (id) => {
    try {
      const medicoEspecialidad = await getElement(id, 'medicoEspecialidad/');
      setMedicoEspecialidad({ ...medicoEspecialidad.data });
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    listGet('medico/').then((data) => setMedicos(data.results));
    listGet('especialidad/').then((data) => setEspecialidades(data.results));
    if (params.id) {
      getMedicoEspecialidad(params.id);
    }
  }, [params.id]);
  return (
    <div className='concurrency especialidades'>
      <div className='container col-xl-4 col-md-6'>
        <h1 className='text-center my-4'>
          {params.id ? 'Update Medico Especialidad' : 'Add Medico Especialidad'}
        </h1>
        <form onSubmit={handleSubmit}>
          <select
            className='form-select form-select-sm mb-3'
            aria-label='.form-select-lg example'
            name='medico_id'
            onChange={handleInputChange}
            value={medicoEspecialidad.medico_id}
          >
            <option selected>Open this select menu</option>
            {medicos
              ? medicos.map((medico) => {
                  return (
                    <option key={medico.id} value={medico.id}>
                      {`${medico.nombre} ${medico.apellido}`}
                    </option>
                  );
                })
              : null}
          </select>
          <select
            className='form-select form-select-sm mb-3'
            aria-label='.form-select-lg example'
            name='especialidad'
            onChange={handleInputChange}
            value={medicoEspecialidad.especialidad}
          >
            <option selected>Open this select menu</option>
            {especialidades
              ? especialidades.map((especialidad) => {
                  return (
                    <option key={especialidad.id} value={especialidad.id}>
                      {especialidad.nombre}
                    </option>
                  );
                })
              : null}
          </select>
          {params.id ? (
            <button type='submit' className='btn btn-block btn-primary mt-3'>
              Update
            </button>
          ) : (
            <button type='submit' className='btn btn-block btn-success mt-3'>
              Register
            </button>
          )}
        </form>
      </div>
    </div>
  );
}
