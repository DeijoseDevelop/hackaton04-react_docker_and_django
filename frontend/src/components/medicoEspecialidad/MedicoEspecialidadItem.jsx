import { useNavigate } from 'react-router-dom';

export default function MedicoEspecialidadItem({
  medicoEspecialidad,
  deleteMedicoEspecialidad,
}) {
  const endpoint = 'medicoEspecialidad/';
  const navigate = useNavigate();
  return (
    <tr>
      <td>{medicoEspecialidad.medico_id}</td>
      <td>{medicoEspecialidad.especialidad}</td>
      <td>
        <button
          onClick={() =>
            navigate(`updateMedicoEspecialidad/${medicoEspecialidad.id}`)
          }
          className='btn btn-primary'
        >
          update
        </button>
      </td>
      <td>
        <button
          onClick={() =>
            deleteMedicoEspecialidad(medicoEspecialidad.id, endpoint)
          }
          className='btn btn-danger'
        >
          delete
        </button>
      </td>
    </tr>
  );
}
