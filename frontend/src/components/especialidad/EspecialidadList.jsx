import { useState, useEffect } from 'react';
import { listGet } from '../../utils/listGet';
import EspecialidadItem from './EspecialidadItem';
import { deleteElement } from '../../utils/deleteElement';

export default function EspecialidadList() {
  const URL = 'especialidad/';

  const [Especialidades, setEspecialidades] = useState([]);

  const getData = async () => {
    try {
      const data = await listGet(URL);
      setEspecialidades(data.results);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className='concurrency especialidades'>
      <div className='container text-center'>
        <h1 className='my-4'>Especialidades</h1>
        <table className='table table-dark table-striped'>
          <thead>
            <tr>
              <th scope='col'>nombre</th>
              <th scope='col'>description</th>
              <th scope='col'>update</th>
              <th scope='col'>delete</th>
            </tr>
          </thead>
          <tbody>
            {Especialidades
              ? Especialidades.map((especialidad) => {
                  return (
                    <EspecialidadItem
                      key={especialidad.id}
                      especialidad={especialidad}
                      deleteEspecialidad={deleteElement}
                    />
                  );
                })
              : null}
          </tbody>
        </table>
      </div>
    </div>
  );
}
