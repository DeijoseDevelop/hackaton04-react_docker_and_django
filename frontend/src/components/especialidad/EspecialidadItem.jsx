import { useNavigate } from 'react-router-dom';

export default function EspecialidadItem({ especialidad, deleteEspecialidad }) {
  const endpoint = 'especialidad/';
  const navigate = useNavigate();
  return (
    <tr>
      <td>{especialidad.nombre}</td>
      <td>{especialidad.description}</td>
      <td>
        <button
          onClick={() => navigate(`updateEspecialidad/${especialidad.id}`)}
          className='btn btn-primary'
        >
          update
        </button>
      </td>
      <td>
        <button
          onClick={() => deleteEspecialidad(especialidad.id, endpoint)}
          className='btn btn-danger'
        >
          delete
        </button>
      </td>
    </tr>
  );
}
