import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import {addEspecialidad} from '../../utils/especialidad/addEspecialidad'
import { updateEspecialidad } from '../../utils/especialidad/updateEspecialidad';
import { getElement } from '../../utils/getElement';

export default function EspecialidadForm() {
  const params = useParams();
  const navigate = useNavigate();
  const initialState = {
    nombre: '',
    description: '',
  };
  const [especialidad, setEspecialidad] = useState(initialState);
  const handleInputChange = (e) => {
    setEspecialidad({
      ...especialidad,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res;
      if (!params.id) {
        res = await addEspecialidad(especialidad);
        if (res.status === 201) {
          setEspecialidad(initialState);
        }
      } else {
        updateEspecialidad(params.id, especialidad);
      }
      navigate('/listEspecialidad');
    } catch (error) {
      console.log(error);
    }
  };
  const getEspecialidad = async (id) => {
    try {
      const especialidad = await getElement(id, 'especialidad/');
      setEspecialidad({ ...especialidad.data });
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    if (params.id) getEspecialidad(params.id);
  }, [params.id]);
  return (
    <div className='concurrency especialidades'>
      <div className='container col-xl-4 col-md-6'>
        <h1 className='text-center my-4'>
          {params.id ? 'Update Especialidad' : 'Add Especialidad'}
        </h1>
        <form onSubmit={handleSubmit}>
          <div className='form-group'>
            <label>Nombre</label>
            <input
              type='text'
              name='nombre'
              className='form-control'
              value={especialidad.nombre}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Description</label>
            <input
              type='text'
              name='description'
              className='form-control'
              value={especialidad.description}
              onChange={handleInputChange}
            />
          </div>
          {params.id ? (
            <button type='submit' className='btn btn-block btn-primary mt-3'>
              Update
            </button>
          ) : (
            <button type='submit' className='btn btn-block btn-success mt-3'>
              Register
            </button>
          )}
        </form>
      </div>
    </div>
  );
}
