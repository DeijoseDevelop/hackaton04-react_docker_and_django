import { Link } from 'react-router-dom';
import { logout } from '../../utils/auth/logout';
import { useNavigate } from 'react-router-dom';

export default function Navbar() {
  const navigate = useNavigate();
  const desloguear = () => {
    logout();
    navigate('/');
  };
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
      <div className='container'>
        <Link className='navbar-brand' to='/home'>
          Home
        </Link>
        <button
          className='navbar-toggler'
          type='button'
          data-bs-toggle='collapse'
          data-bs-target='#navbarNavAltMarkup'
          aria-controls='navbarNavAltMarkup'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon'></span>
        </button>
        {localStorage.getItem('token') ? (
          <button onClick={() => desloguear()} className='btn btn-danger'>
            Salir
          </button>
        ) : null}
        <div className='collapse navbar-collapse' id='navbarNavAltMarkup'>
          <div className='navbar-nav ms-auto'>
            <ul className='d-flex gap-4'>
              <li className='nav-item dropdown'>
                <Link
                  className='nav-link dropdown-toggle'
                  to='/listMedico'
                  id='navbarScrollingDropdown'
                  role='button'
                  data-bs-toggle='dropdown'
                  aria-expanded='false'
                >
                  Medicos
                </Link>
                <ul
                  className='dropdown-menu'
                  aria-labelledby='navbarScrollingDropdown'
                >
                  <li>
                    <Link className='dropdown-item' to='/listMedico'>
                      Medico List
                    </Link>
                  </li>
                  <li>
                    <Link className='dropdown-item' to='/medicoForm'>
                      Add Medico
                    </Link>
                  </li>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <Link
                  className='nav-link dropdown-toggle'
                  to='/listPaciente'
                  id='navbarScrollingDropdown'
                  role='button'
                  data-bs-toggle='dropdown'
                  aria-expanded='false'
                >
                  Pacientes
                </Link>
                <ul
                  className='dropdown-menu'
                  aria-labelledby='navbarScrollingDropdown'
                >
                  <li>
                    <Link className='dropdown-item' to='/listPaciente'>
                      Paciente List
                    </Link>
                  </li>
                  <li>
                    <Link className='dropdown-item' to='/pacienteForm'>
                      Add Paciente
                    </Link>
                  </li>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <Link
                  className='nav-link dropdown-toggle'
                  to='/listPaciente'
                  id='navbarScrollingDropdown'
                  role='button'
                  data-bs-toggle='dropdown'
                  aria-expanded='false'
                >
                  Especialidades
                </Link>
                <ul
                  className='dropdown-menu'
                  aria-labelledby='navbarScrollingDropdown'
                >
                  <li>
                    <Link className='dropdown-item' to='/listEspecialidad'>
                      Especialidade List
                    </Link>
                  </li>
                  <li>
                    <Link className='dropdown-item' to='/especialidadForm'>
                      Add Especialidade
                    </Link>
                  </li>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <Link
                  className='nav-link dropdown-toggle'
                  to='/listHorario'
                  id='navbarScrollingDropdown'
                  role='button'
                  data-bs-toggle='dropdown'
                  aria-expanded='false'
                >
                  Horarios
                </Link>
                <ul
                  className='dropdown-menu'
                  aria-labelledby='navbarScrollingDropdown'
                >
                  <li>
                    <Link className='dropdown-item' to='/listHorario'>
                      Horario List
                    </Link>
                  </li>
                  <li>
                    <Link className='dropdown-item' to='/horarioForm'>
                      Add Horario
                    </Link>
                  </li>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <Link
                  className='nav-link dropdown-toggle'
                  to='/listHorarios'
                  id='navbarScrollingDropdown'
                  role='button'
                  data-bs-toggle='dropdown'
                  aria-expanded='false'
                >
                  Medicos Especialidades
                </Link>
                <ul
                  className='dropdown-menu'
                  aria-labelledby='navbarScrollingDropdown'
                >
                  <li>
                    <Link
                      className='dropdown-item'
                      to='/listMedicosEspecialidades'
                    >
                      Medicos Especialidades List
                    </Link>
                  </li>
                  <li>
                    <Link
                      className='dropdown-item'
                      to='/medicosEspecialidadesForm'
                    >
                      Add Medicos Especialidades
                    </Link>
                  </li>
                </ul>
              </li>
              <li className='nav-item dropdown'>
                <Link
                  className='nav-link dropdown-toggle'
                  to='/listCitas'
                  id='navbarScrollingDropdown'
                  role='button'
                  data-bs-toggle='dropdown'
                  aria-expanded='false'
                >
                  Citas
                </Link>
                <ul
                  className='dropdown-menu'
                  aria-labelledby='navbarScrollingDropdown'
                >
                  <li>
                    <Link className='dropdown-item' to='/listCitas'>
                      Citas List
                    </Link>
                  </li>
                  <li>
                    <Link className='dropdown-item' to='/citaForm'>
                      Add Citas
                    </Link>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}
