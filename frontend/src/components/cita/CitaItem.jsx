import { useNavigate } from 'react-router-dom';

export default function CitaItem({ cita, deleteCita }) {
  const endpoint = 'cita/';
  const navigate = useNavigate();
  return (
    <tr>
      <td>{cita.medico_id}</td>
      <td>{cita.paciente_id}</td>
      <td>
        {cita.estado ? (
          <span className='badge bg-success'>True</span>
        ) : (
          <span className='badge bg-danger'>False</span>
        )}
      </td>
      <td>
        <button
          onClick={() => navigate(`updateCita/${cita.id}`)}
          className='btn btn-primary'
        >
          update
        </button>
      </td>
      <td>
        <button
          onClick={() => deleteCita(cita.id, endpoint)}
          className='btn btn-danger'
        >
          delete
        </button>
      </td>
    </tr>
  );
}