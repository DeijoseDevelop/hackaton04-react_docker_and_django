import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { addCita } from '../../utils/cita/addCita';
import { updateCita } from '../../utils/cita/updateCita';
import { getElement } from '../../utils/getElement';
import { listGet } from '../../utils/listGet';

export default function CitaForm() {
  const params = useParams();
  const navigate = useNavigate();
  const initialState = {
    medico_id: '',
    paciente_id: '',
    estado: false,
  }
    const [cita, setCita] = useState(initialState);
    const [medicos, setMedicos] = useState([]);
    const [pacientes, setPacientes] = useState([]);

    const handleInputChange = (e) => {
        setCita({
            ...cita,
            [e.target.name]: e.target.value,
        });
    }
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            let res;
            if (!params.id) {
                res = await addCita(cita);
                if (res.status === 201) {
                    setCita(initialState);
                }
            } else {
                updateCita(params.id, cita);
            }
            navigate('/listCitas');
        } catch (error) {
            console.log(error);
        }
    }
    const getCita = async (id) => {
        try {
            const cita = await getElement(id, 'cita/');
            setCita({ ...cita.data });
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(()=>{
        listGet('medico/').then((data) => setMedicos(data.results));
        listGet('paciente/').then((data) => setPacientes(data.results));
        if (params.id) {
            getCita(params.id);
        }
    }, [params.id]);
    return (
      <div className='concurrency citas'>
        <div className='container col-xl-4 col-md-6'>
          <h1 className='text-center my-4'>
            {params.id ? 'Update Cita' : 'Add Cita'}
          </h1>
          <form onSubmit={handleSubmit}>
            <select
              className='form-select form-select-sm mb-3'
              aria-label='.form-select-lg example'
              name='medico_id'
              onChange={handleInputChange}
              value={cita.medico_id}
            >
              <option selected>Open this select menu</option>
              {medicos
                ? medicos.map((medico) => {
                    return (
                      <option key={medico.id} value={medico.id}>
                        {`${medico.nombre} ${medico.apellido}`}
                      </option>
                    );
                  })
                : null}
            </select>
            <select
              className='form-select form-select-sm mb-3'
              aria-label='.form-select-lg example'
              name='paciente_id'
              onChange={handleInputChange}
              value={cita.paciente_id}
            >
              <option selected>Open this select menu</option>
              {pacientes
                ? pacientes.map((paciente) => {
                    return (
                      <option key={paciente.id} value={paciente.id}>
                        {`${paciente.nombres} ${paciente.apellidos}`}
                      </option>
                    );
                  })
                : null}
            </select>
            <div className='form-check'>
              <input
                className='form-check-input'
                type='radio'
                name='estado'
                id='flexRadioDefault1'
                value={true}
                onChange={handleInputChange}
              />
              <label className='form-check-label' htmlFor='flexRadioDefault1'>
                Estado Activo
              </label>
            </div>
            <div className='form-check'>
              <input
                className='form-check-input'
                type='radio'
                name='estado'
                id='flexRadioDefault2'
                value={false}
                onChange={handleInputChange}
              />
              <label className='form-check-label' htmlFor='flexRadioDefault2'>
                Estado Inactivo
              </label>
            </div>
            {params.id ? (
              <button type='submit' className='btn btn-block btn-primary mt-3'>
                Update
              </button>
            ) : (
              <button type='submit' className='btn btn-block btn-success mt-3'>
                Register
              </button>
            )}
          </form>
        </div>
      </div>
    );
}
