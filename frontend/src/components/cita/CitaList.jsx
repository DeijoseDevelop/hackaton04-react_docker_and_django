import { useState, useEffect } from 'react';
import { listGet } from '../../utils/listGet';
import CitaItem from './CitaItem';
import { deleteElement } from '../../utils/deleteElement';

export default function CitaList() {
  const URL = 'cita/';

  const [citas, setCitas] = useState([]);

  const getData = async () => {
    try {
      const data = await listGet(URL);
      setCitas(data.results);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className='concurrency citas'>
      <div className='container text-center'>
        <h1 className='my-4'>Citas</h1>
        <table className='table table-dark table-striped'>
          <thead>
            <tr>
              <th scope='col'>medico_id</th>
              <th scope='col'>paciente_id</th>
              <th scope='col'>estado</th>
              <th scope='col'>update</th>
              <th scope='col'>delete</th>
            </tr>
          </thead>
          <tbody>
            {citas
              ? citas.map((cita) => {
                  return (
                    <CitaItem
                      key={cita.id}
                      cita={cita}
                      deleteCita={deleteElement}
                    />
                  );
                })
              : null}
          </tbody>
        </table>
      </div>
    </div>
  );
}
