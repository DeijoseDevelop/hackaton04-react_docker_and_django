import {useContext} from 'react';
import {Link} from 'react-router-dom';
import {AuthContext} from '../context/AuthContext'

export default function Home(){
  const authContext = useContext(AuthContext);

  console.log(authContext.session);
    return (
      <div className='concurrency auth'>
        <div className='container mt-5 text-center w-25'>
          <h1 className='my-4'>Routes</h1>
          <Link
            className='list-group-item list-group-item-action  list-group-item-primary'
            to='/listMedico'
          >
            Medicos
          </Link>
          <Link
            className='list-group-item list-group-item-action  list-group-item-secondary'
            to='/listPaciente'
          >
            Pacientes
          </Link>
          <Link
            className='list-group-item list-group-item-action  list-group-item-success'
            to='/listEspecialidad'
          >
            Especialidades
          </Link>
          <Link
            className='list-group-item list-group-item-action  list-group-item-danger'
            to='/listHorarios'
          >
            Horarios
          </Link>
          <Link
            className='list-group-item list-group-item-action  list-group-item-warning'
            to='/listMedicosEspecialidades'
          >
            Medico Especialidades
          </Link>
          <Link
            className='list-group-item list-group-item-action  list-group-item-info'
            to='/listCitas'
          >
            Citas
          </Link>
        </div>
      </div>
    );
}