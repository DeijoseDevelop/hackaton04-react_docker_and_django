import { useState, useEffect } from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import { addMedico } from '../../utils/medico/addMedico';
import {updateMedico} from '../../utils/medico/updateMedico';
import { getElement } from '../../utils/getElement';

export default function MedicoForm() {
  const params = useParams();
  const navigate = useNavigate();
  const initialState = {
    nombre: '',
    apellido: '',
    dni: '',
    direccion: '',
    correo: '',
    telefono: '',
    sexo: '',
    num_colegiatura: '',
    fecha_nacimiento: '',
  };
  const [medico, setMedico] = useState(initialState);

  const handleInputChange = (e) => {
    setMedico({
      ...medico,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let res;
      if(!params.id){
        res = await addMedico(medico);
        if(res.status === 201){
          setMedico(initialState)
        }
      } else{
        updateMedico(params.id, medico);
      }
      navigate('/listMedico');
    } catch (error) {
      console.log(error);
    }
  };

  const getMedico = async (id) => {
    try {
      const medico = await getElement(id, 'medico/');
      setMedico({...medico.data});
    } catch (error) {
      console.log(error);
    }
  }
  useEffect(() => {
    if(params.id){
      getMedico(params.id);
    }
  }, [params.id]);
  return (
    <div className='concurrency medico'>
      <div className='container col-xl-4 col-md-6'>
        <h1 className='text-center my-4'>
          {params.id ? 'Update Medico' : 'Add Medico'}
        </h1>
        <form onSubmit={handleSubmit}>
          <div className='form-group'>
            <label>Nombre</label>
            <input
              type='text'
              name='nombre'
              className='form-control'
              value={medico.nombre}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Apellido</label>
            <input
              type='text'
              name='apellido'
              className='form-control'
              value={medico.apellido}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>DNI</label>
            <input
              type='text'
              name='dni'
              className='form-control'
              value={medico.dni}
              onChange={handleInputChange}
              maxLength='8'
            />
          </div>
          <div className='form-group'>
            <label>Dirección</label>
            <input
              type='text'
              name='direccion'
              className='form-control'
              value={medico.direccion}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Correo</label>
            <input
              type='email'
              name='correo'
              className='form-control'
              value={medico.correo}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Telefono</label>
            <input
              type='text'
              name='telefono'
              className='form-control'
              value={medico.telefono}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-check'>
            <input
              className='form-check-input'
              type='radio'
              name='sexo'
              id='flexRadioDefault1'
              value='M'
              onChange={handleInputChange}
            />
            <label className='form-check-label' htmlFor='flexRadioDefault1'>
              Masculino
            </label>
          </div>
          <div className='form-check'>
            <input
              className='form-check-input'
              type='radio'
              name='sexo'
              id='flexRadioDefault2'
              value='F'
              onChange={handleInputChange}
            />
            <label className='form-check-label' htmlFor='flexRadioDefault2'>
              Femenino
            </label>
          </div>
          <div className='form-group'>
            <label>Num_colegiatura</label>
            <input
              type='text'
              name='num_colegiatura'
              className='form-control'
              value={medico.num_colegiatura}
              onChange={handleInputChange}
            />
          </div>
          <div className='form-group'>
            <label>Fecha Nacimiento</label>
            <input
              type='date'
              name='fecha_nacimiento'
              className='form-control'
              value={medico.fecha_nacimiento}
              onChange={handleInputChange}
            />
          </div>
          {params.id ? (
            <button type='submit' className='btn btn-block btn-primary mt-3'>
              Update
            </button>
          ) : (
            <button type='submit' className='btn btn-block btn-success mt-3'>
              Register
            </button>
          )}
        </form>
      </div>
    </div>
  );
}
