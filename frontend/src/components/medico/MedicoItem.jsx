import {useNavigate} from 'react-router-dom';

export default function MedicoItem({medico, deleteMedico}){
  const endpoint = 'medico/';
  const navigate = useNavigate();
    return (
      <tr>
        <td>{medico.nombre}</td>
        <td>{medico.apellido}</td>
        <td>{medico.dni}</td>
        <td>{medico.direccion}</td>
        <td>{medico.correo}</td>
        <td>{medico.telefono}</td>
        <td>{medico.sexo}</td>
        <td>{medico.num_colegiatura}</td>
        <td>{medico.fecha_nacimiento}</td>
        <td>
          <button onClick={()=> navigate(`updateMedico/${medico.id}`)} className='btn btn-primary'>update</button>
        </td>
        <td>
          <button onClick={()=> deleteMedico(medico.id, endpoint)} className='btn btn-danger'>
            delete
          </button>
        </td>
      </tr>
    );
}