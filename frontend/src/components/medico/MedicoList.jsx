import { useState, useEffect } from 'react';
import { listGet } from '../../utils/listGet';
import MedicoItem from './MedicoItem';
import { deleteElement } from '../../utils/deleteElement';

export default function MedicoList() {
  const URL = 'medico/';

  const [medicos, setMedicos] = useState([]);

  const getData = async () => {
    try {
      const data = await listGet(URL);
      setMedicos(data.results);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <div className='concurrency medico'>
      <div className='px-5 text-center'>
        <h1 className='my-4'>Medicos</h1>
        <table className='table table-dark table-striped'>
          <thead>
            <tr>
              <th scope='col'>nombre</th>
              <th scope='col'>apellido</th>
              <th scope='col'>dni</th>
              <th scope='col'>direccion</th>
              <th scope='col'>correo</th>
              <th scope='col'>telefono</th>
              <th scope='col'>sexo</th>
              <th scope='col'>num_colegiatura</th>
              <th scope='col'>fecha_nacimiento</th>
              <th scope='col'>update</th>
              <th scope='col'>delete</th>
            </tr>
          </thead>
          <tbody>
            {medicos
              ? medicos.map((medico) => {
                  return (
                    <MedicoItem
                      key={medico.id}
                      medico={medico}
                      deleteMedico={deleteElement}
                    />
                  );
                })
              : null}
          </tbody>
        </table>
      </div>
    </div>
  );
}
