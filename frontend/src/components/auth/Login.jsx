import { useState, useContext } from 'react';
import { getToken } from '../../utils/auth/getToken';
import useAuthenticateHook from '../../hooks/useAuthenticateHook';
import { Link, useNavigate } from 'react-router-dom';
import {AuthContext} from '../../context/AuthContext';

export default function Login() {
    const authContext = useContext(AuthContext);
  const { login } = useAuthenticateHook();
  const navigate = useNavigate();

  const initialState = {
    username: '',
    password: '',
  }
  const [user, setUser] = useState(initialState);

  
  const handleInputChange = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value,
    });
  };

  const handlingToken = (tokens)=>{
    localStorage.getItem('token') ? localStorage.removeItem('token') : localStorage.setItem('token', tokens.access);
  }


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const login2 = await login(user);
      authContext.changeSession({
        isAuthenticated: true,
        token: login2.tokens.access,
        refreshToken: login2.tokens.refresh,
      });
      handlingToken(login2.tokens);
      navigate('/home');
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="concurrency auth">
      <div className='transparency container mx-auto p-4 rounded-3 col-xl-3 col-lg-6 col-md-5'>
        <h1 className='text-center'>Login</h1>
        <form method='post' onSubmit={handleSubmit}>
          <div className='mb-3'>
            <label htmlFor='username'>Username</label>
            <input
              type='text'
              className='form-control'
              id='username'
              name='username'
              placeholder='Username'
              onChange={handleInputChange}
            />
          </div>
          <div className='mb-3'>
            <label htmlFor='password'>Password</label>
            <input
              type='password'
              className='form-control'
              id='password'
              name='password'
              placeholder='Password'
              onChange={handleInputChange}
            />
          </div>
          <div className='d-flex justify-content-around'>
            <button className='btn btn-success' type='submit'>
              Login
            </button>
            <Link to='/register' className='btn btn-primary'>
              Register Form
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
}
