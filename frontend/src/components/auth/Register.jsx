import { useState } from "react";
import {register} from '../../utils/auth/register';
import { Link } from 'react-router-dom';
import { useNavigate } from "react-router-dom";

export default function Register(){
  const navigate = useNavigate();
    const initialState = {
        username: '',
        first_name: '',
        last_name: '',
        email: '',
        password: '',
    }
    const [user, setUser] = useState(initialState);

    const handleInputChange = (e)=>{
        setUser({
            ...user,
            [e.target.name]: e.target.value,
        });
    }

    const handleSubmit = async (e)=>{
        e.preventDefault();
        try {
            await register(user);
            navigate('/');
        } catch (error) {
            console.log(error);
        }
    }

    return (
      <div className="concurrency auth">
        <div className='transparency container p-4 rounded-3 col-xl-3 col-lg-6 col-md-5'>
          <h1 className='text-center'>Register</h1>
          <form method='post' onSubmit={handleSubmit}>
            <div className='mb-3'>
              <label htmlFor='username'>Username</label>
              <input
                type='text'
                className='form-control'
                id='username'
                name='username'
                placeholder='Username'
                onChange={handleInputChange}
              />
            </div>
            <div className='mb-3'>
              <label htmlFor='username'>First Name</label>
              <input
                type='text'
                className='form-control'
                id='first_name'
                name='first_name'
                placeholder='First Name'
                onChange={handleInputChange}
              />
            </div>
            <div className='mb-3'>
              <label htmlFor='username'>Last Name</label>
              <input
                type='text'
                className='form-control'
                id='last_name'
                name='last_name'
                placeholder='Last Name'
                onChange={handleInputChange}
              />
            </div>
            <div className='mb-3'>
              <label htmlFor='username'>Email</label>
              <input
                type='email'
                className='form-control'
                id='email'
                name='email'
                placeholder='Email'
                onChange={handleInputChange}
              />
            </div>
            <div className='mb-3'>
              <label htmlFor='password'>Password</label>
              <input
                type='password'
                className='form-control'
                id='password'
                name='password'
                placeholder='Password'
                onChange={handleInputChange}
              />
            </div>
            <div className='d-flex justify-content-around'>
              <button className='btn btn-success' type='submit'>
                Register
              </button>
              <Link to='/' className='btn btn-primary'>
                Login
              </Link>
            </div>
          </form>
        </div>
      </div>
    );
}