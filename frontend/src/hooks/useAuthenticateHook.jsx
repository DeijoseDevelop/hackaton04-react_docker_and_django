import { useContext } from 'react';
import { instanceAuthApi } from '../utils/instanceApi';
import { AuthContext } from '../context/AuthContext';
import { getToken } from '../utils/auth/getToken';

/**
 * A function that takes in a username and password and returns a response and tokens
 * @returns An object with a login function.
 */
const useAuthenticateHook = () => {

  /**
   * A function that takes in a username and password and returns a response and tokens.
   * @returns {data: response.data, tokens}
   */
  const login = async ({ username, password }) => {
    try {
      const data = JSON.stringify({
        username,
        password,
      });
      const tokens = await getToken({ username, password });
      const response = await instanceAuthApi.post('login/', data)
      return {data: response.data, tokens};
    } catch (error) {
      console.log(error);
    }
  };
  return { login };
};
export default useAuthenticateHook;
