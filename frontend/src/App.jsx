import Navbar from './components/navbar/Navbar';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import {AuthProvider, sessionInfo} from './context/AuthContext'
{
  /*   Auth   */
}
import Login from './components/auth/Login';
import Register from './components/auth/Register';
{
  /*   Home   */
}
import Home from './components/Home';
{
  /*  Medicos Routes  */
}
import MedicoList from './components/medico/MedicoList';
import MedicoForm from './components/medico/MedicoForm';
{
  /*  Pacientes Routes   */
}
import PacienteList from './components/paciente/PacienteList';
import PacienteForm from './components/paciente/PacienteForm';
{
  /*  Especialidad Routes   */
}
import EspecialidadList from './components/especialidad/EspecialidadList';
import EspecialidadForm from './components/especialidad/EspecialidadForm';
{
  /*  Horario Routes   */
}
import HorarioList from './components/horario/HorarioList';
import HorarioForm from './components/horario/HorarioForm';
{
  /*  Medico Especialidad Routes   */
}
import MedicoEspecialidadList from './components/medicoEspecialidad/MedicoEspecialidadList';
import MedicoEspecialidadForm from './components/medicoEspecialidad/MedicoEspecialidadForm';
{
  /*  Citas Routes   */
}
import CitaList from './components/cita/CitaList';
import CitaForm from './components/cita/CitaForm';

function App() {
  return (
    <AuthProvider initialValueSession={sessionInfo}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          {/*  Auth  */}
          <Route path='/' element={<Login />} />
          <Route path='/register' element={<Register />} />

          {/*Home*/}
          <Route path='/home' element={<Home />} />

          {/*  Medicos Routes  */}
          <Route path='/listMedico' element={<MedicoList />} />
          <Route path='/listMedico/updateMedico/:id' element={<MedicoForm />} />
          <Route path='/medicoForm' element={<MedicoForm />} />

          {/*  Pacientes Routes   */}
          <Route path='/listPaciente' element={<PacienteList />} />
          <Route
            path='/listPaciente/updatePaciente/:id'
            element={<PacienteForm />}
          />
          <Route path='/pacienteForm' element={<PacienteForm />} />

          {/*  Especialidad Routes   */}
          <Route path='/listEspecialidad' element={<EspecialidadList />} />
          <Route path='/especialidadForm' element={<EspecialidadForm />} />
          <Route
            path='/listEspecialidad/updateEspecialidad/:id'
            element={<EspecialidadForm />}
          />

          {/*  Horario Routes   */}
          <Route path='/listHorario' element={<HorarioList />} />
          <Route
            path='/listHorario/updateHorario/:id'
            element={<HorarioForm />}
          />
          <Route path='/horarioForm' element={<HorarioForm />} />

          {/*  Medicos Especialidades Routes   */}
          <Route
            path='/listMedicosEspecialidades'
            element={<MedicoEspecialidadList />}
          />
          <Route
            path='/medicosEspecialidadesForm'
            element={<MedicoEspecialidadForm />}
          />
          <Route
            path='/listMedicosEspecialidades/updateMedicoEspecialidad/:id'
            element={<MedicoEspecialidadForm />}
          />

          {/*  Citas Routes   */}
          <Route path='/listCitas' element={<CitaList />} />
          <Route path='/citaForm' element={<CitaForm />} />
          <Route path='/listCitas/updateCita/:id' element={<CitaForm />} />
        </Routes>
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
