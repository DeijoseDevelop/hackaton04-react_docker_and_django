import { instanceApi } from '../instanceApi';

/**
 * It updates a patient's data
 * @param id - The id of the paciente you want to update.
 * @param newPaciente - {
 * @returns The response of the request.
 */
export const updatePaciente = async (id, newPaciente) => {
  try {
    const data = JSON.stringify({
      nombres: String(newPaciente.nombres).trim(),
      apellidos: String(newPaciente.apellidos).trim(),
      dni: String(newPaciente.dni).trim(),
      direccion: String(newPaciente.direccion).trim(),
      telefono: String(newPaciente.telefono).trim(),
      sexo: String(newPaciente.sexo).trim(),
      fecha_nacimiento: String(newPaciente.fecha_nacimiento).trim(),
    });
    return await instanceApi.put(`paciente/${id}/`, data);
  } catch (error) {
    console.log(error);
  }
};
