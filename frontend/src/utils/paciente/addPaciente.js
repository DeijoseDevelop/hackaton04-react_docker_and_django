import { instanceApi } from '../instanceApi';

/**
 * It sends a POST request to the API with the data of the new patient
 * @param newPaciente - This is the object that contains the data that will be sent to the server.
 * @returns The response of the request.
 */
export const addPaciente = async (newPaciente) => {
  const data = JSON.stringify({
    nombres: String(newPaciente.nombres).trim(),
    apellidos: String(newPaciente.apellidos).trim(),
    dni: String(newPaciente.dni).trim(),
    direccion: String(newPaciente.direccion).trim(),
    telefono: String(newPaciente.telefono).trim(),
    sexo: String(newPaciente.sexo).trim(),
    fecha_nacimiento: String(newPaciente.fecha_nacimiento).trim(),
  });
  return await instanceApi.post('paciente/', data);
};
