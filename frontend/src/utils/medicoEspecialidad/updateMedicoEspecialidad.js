import { instanceApi } from '../instanceApi';

/**
 * It updates a medicoEspecialidad in the database
 * @param id - The id of the medicoEspecialidad you want to update.
 * @param newMedicoEspecialidad - {
 * @returns The data is being returned.
 */
export const updateMedicoEspecialidad = async (id, newMedicoEspecialidad) => {
  try {
    const data = JSON.stringify({
      medico_id: Number(String(newMedicoEspecialidad.medico_id).trim()),
      especialidad_id: Number(
        String(newMedicoEspecialidad.especialidad_id).trim()
      ),
    });
    return await instanceApi.put(`medicoEspecialidad/${id}/`, data);
  } catch (error) {
    console.log(error);
  }
};
