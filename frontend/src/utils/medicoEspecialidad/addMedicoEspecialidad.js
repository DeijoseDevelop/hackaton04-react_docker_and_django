import { instanceApi } from '../instanceApi';

/**
 * It sends a POST request to the API with the data of the new medicoEspecialidad
 * @param newMedicoEspecialidad - {
 * @returns The response from the server.
 */
export const addMedicoEspecialidad = async (newMedicoEspecialidad) => {
  const data = JSON.stringify({
    medico_id: Number(String(newMedicoEspecialidad.medico_id).trim()),
    especialidad: Number(
      String(newMedicoEspecialidad.especialidad_id).trim()
    ),
  });
  return await instanceApi.post(`medicoEspecialidad/`, data);
};
