import { instanceApi } from './instanceApi';

/**
 * It's an async function that uses the axios instance to make a get request to the URL passed in as a
 * parameter
 * @param URL - The URL of the API endpoint you want to call.
 * @returns The response.data is being returned.
 */
export async function listGet(URL) {
  try {
    const response = await instanceApi.get(URL);
    return response.data;
  } catch (error) {
    console.log(error);
  }
}
