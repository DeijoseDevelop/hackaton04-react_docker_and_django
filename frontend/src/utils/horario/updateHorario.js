import { instanceApi } from '../instanceApi';

/**
 * It updates a horario in the database
 * @param id - The id of the horario you want to update.
 * @param newHorario - {
 * @returns The data is being returned.
 */
export const updateHorario = async (id, newHorario) => {
  try {
    const data = JSON.stringify({
      medico_id: Number(String(newHorario.medico_id).trim()),
      activo: newHorario.activo == 'false' ? false : true,
      fecha_registro: String(newHorario.fecha_registro).trim(),
      usuario_registro: String(newHorario.usuario_registro).trim(),
      fecha_modificacion: String(newHorario.fecha_modificacion).trim(),
      usuario_modificacion: String(newHorario.usuario_modificacion).trim(),
      fecha_atencion: String(newHorario.fecha_atencion).trim(),
      inicio_atencion: String(newHorario.inicio_atencion).trim(),
      fin_atencion: String(newHorario.fin_atencion).trim(),
    });
    return await instanceApi.put(`horario/${id}/`, data);
  } catch (error) {
    console.log(error);
  }
};
