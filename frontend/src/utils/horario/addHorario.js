import {instanceApi} from '../instanceApi';

/**
 * It takes a newHorario object as a parameter, converts it to a string, and then sends it to the
 * server
 * @param newHorario - {
 * @returns The response of the request.
 */
export const addHorario = async (newHorario) => {
    const data = JSON.stringify({
        medico_id: Number(String(newHorario.medico_id).trim()),
        activo: Boolean(String(newHorario.activo).trim()),
        fecha_registro: String(newHorario.fecha_registro).trim(),
        usuario_registro: String(newHorario.usuario_registro).trim(),
        fecha_modificacion: String(newHorario.fecha_modificacion).trim(),
        usuario_modificacion: String(newHorario.usuario_modificacion).trim(),
        fecha_atencion: String(newHorario.fecha_atencion).trim(),
        inicio_atencion: String(newHorario.inicio_atencion).trim(),
        fin_atencion: String(newHorario.fin_atencion).trim(),
    });
    return await instanceApi.post('horario/', data);
}