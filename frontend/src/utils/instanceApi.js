import { useContext } from 'react';
import axios from 'axios';
import { AuthContext } from '../context/AuthContext';




export const instanceApi = axios.create({
    baseURL: 'http://localhost:8000/api/',
    /* The above code is checking if the token is in local storage and if it is, it is setting the
    authorization header to be a bearer token with the token as the value. */
    headers: {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('token') ? `Bearer ${localStorage.getItem('token')}` : '',
    }
});

export const instanceAuthApi = axios.create({
    baseURL: 'http://localhost:8000/api/auth/',
    headers: {
        'Content-Type': 'application/json',
    }
});