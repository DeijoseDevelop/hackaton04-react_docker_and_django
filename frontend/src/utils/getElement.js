import { instanceApi } from "./instanceApi";

/**
 * It takes an id and an endpoint and returns the data from the API
 * @param id - the id of the element you want to get
 * @param endpoint - the endpoint of the API you're trying to access.
 * @returns The response from the API call.
 */
export const getElement = async (id, endpoint) => {
  try {
    return await instanceApi.get(`${endpoint}${id}/`);
  } catch (error) {
    console.log(error);
  }
}