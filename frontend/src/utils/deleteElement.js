import {instanceApi} from './instanceApi';

/**
 * It takes an id and an endpoint as arguments, and then it uses the axios instance to delete the
 * element with the given id from the given endpoint
 * @param id - the id of the element you want to delete
 * @param endpoint - The endpoint of the API you want to use.
 */
export const deleteElement = async (id, endpoint)=>{
    try {
        await instanceApi.delete(`${endpoint}${id}/`);
        location.reload();
    } catch (error) {
        console.log(error);
    }
}