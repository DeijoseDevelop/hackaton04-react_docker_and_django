import { instanceApi } from "../instanceApi";

/**
 * It updates an especialidad in the database
 * @param id - The id of the especialidad you want to update.
 * @param newEspecialidad - {
 * @returns The response of the request.
 */
export const updateEspecialidad = async (id, newEspecialidad) => {
    try {
        const data = JSON.stringify({
          nombre: String(newEspecialidad.nombre).trim(),
          description: String(newEspecialidad.description).trim(),
        });
        return await instanceApi.put(`especialidad/${id}/`, data);
    } catch (error) {
        console.log(error);
    }
}