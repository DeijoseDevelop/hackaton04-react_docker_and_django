import { instanceApi } from "../instanceApi";

/**
 * It sends a POST request to the API with the data of the new especialidad
 * @param newEspecialidad - {
 * @returns The response of the request.
 */
export const addEspecialidad = async (newEspecialidad) => {
    const data = JSON.stringify({
        nombre: String(newEspecialidad.nombre).trim(),
        description: String(newEspecialidad.description).trim(),
    });
    return await instanceApi.post('especialidad/', data)
}