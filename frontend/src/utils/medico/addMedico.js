import { instanceApi } from "../instanceApi";

/**
 * It takes a newMedico object as a parameter, converts it to a string, and sends it to the server
 * @param newMedico - {
 * @returns The response of the request.
 */
export const addMedico = async (newMedico) => {
  const data = JSON.stringify({
    nombre: String(newMedico.nombre).trim(),
    apellido: String(newMedico.apellido).trim(),
    dni: String(newMedico.dni).trim(),
    direccion: String(newMedico.direccion).trim(),
    correo: String(newMedico.correo).trim(),
    telefono: String(newMedico.telefono).trim(),
    sexo: String(newMedico.sexo).trim(),
    num_colegiatura: String(newMedico.num_colegiatura).trim(),
    fecha_nacimiento: String(newMedico.fecha_nacimiento).trim(),
  });
  return await instanceApi.post('medico/', data);
}