import {instanceApi} from '../instanceApi';

/**
 * It takes an id and a newMedico object as parameters, and returns a promise that resolves to the
 * response of the PUT request to the API
 * @param id - The id of the medico you want to update.
 * @param newMedico - {
 * @returns The response of the request.
 */
export const updateMedico = async (id, newMedico) => {
    try {
        const data = JSON.stringify({
        nombre: String(newMedico.nombre).trim(),
        apellido: String(newMedico.apellido).trim(),
        dni: String(newMedico.dni).trim(),
        direccion: String(newMedico.direccion).trim(),
        correo: String(newMedico.correo).trim(),
        telefono: String(newMedico.telefono).trim(),
        sexo: String(newMedico.sexo).trim(),
        num_colegiatura: String(newMedico.num_colegiatura).trim(),
        fecha_nacimiento: String(newMedico.fecha_nacimiento).trim(),
        });
        return await instanceApi.put(`medico/${id}/`, data);
    } catch (error) {
        console.log(error);
    }
}