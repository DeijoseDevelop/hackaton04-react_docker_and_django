import { instanceApi } from '../instanceApi';

/**
 * It takes an id and a newCita object as parameters, and returns a promise that resolves to the
 * response of the PUT request to the API
 * @param id - The id of the cita you want to update.
 * @param newCita - {
 * @returns The data is being returned.
 */
export const updateCita = async (id, newCita) => {
  console.log(newCita);
  try {
    const data = JSON.stringify({
      medico_id: Number(String(newCita.medico_id).trim()),
      paciente_id: Number(String(newCita.paciente_id).trim()),
      estado: newCita.estado == 'false' ? false : true,
    });
    return await instanceApi.put(`cita/${id}/`, data);
  } catch (error) {
    console.log(error);
  }
};
