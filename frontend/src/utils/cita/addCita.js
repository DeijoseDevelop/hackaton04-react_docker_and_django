import { instanceApi } from '../instanceApi';
/**
 * It sends a POST request to the API with the data of the new appointment
 * @param newCita - This is the object that contains the data that will be sent to the API.
 * @returns The response of the request.
 */

export const addCita = async (newCita) => {
  const data = JSON.stringify({
    medico_id: Number(String(newCita.medico_id).trim()),
    paciente_id: Number(String(newCita.paciente_id).trim()),
    estado: Boolean(String(newCita.estado).trim()),
  });
  return await instanceApi.post('cita/', data);
};
