import axios from "axios";

const URL = "http://localhost:8000/api/token/";

/**
 * It sends a POST request to the URL with the username and password, and returns the response data
 * @returns The token is being returned.
 */
export const getToken = async ({ username, password})=>{
    const response = await axios.post(URL, {
      username,
      password,
    });
    return await response.data;
}