import {instanceAuthApi} from '../instanceApi'

/**
 * It removes the token from local storage and then makes a post request to the logout endpoint.
 * @returns The token is being removed from local storage and the user is being logged out.
 */
export const logout = ()=>{
    localStorage.removeItem('token');
    return instanceAuthApi.post('logout/')

}