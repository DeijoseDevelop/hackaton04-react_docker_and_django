import { instanceAuthApi } from '../../utils/instanceApi';


/**
 * It takes in an object with the user's credentials, stringifies it, and sends it to the server
 * @returns The response.data is being returned.
 */
export const register = async ({ username, first_name, last_name, email, password }) => {
  const data = JSON.stringify({
    username,
    first_name,
    last_name,
    email,
    password,
  });
  const response = await instanceAuthApi.post('signup/', data);
  return response.data;
};
