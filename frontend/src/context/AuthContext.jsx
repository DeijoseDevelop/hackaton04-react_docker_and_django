import { useState, createContext } from 'react';

const sessionInfo = {
  isAuthenticated: false,
  token: '',
  refreshToken: '',
};

const AuthContext = createContext(sessionInfo);

const { Provider, Consumer } = AuthContext;

/**
 * AuthProvider is a function that takes in initialValueSession and children and returns a Provider
 * with a value of session and changeSession
 * @returns The AuthProvider is returning a Provider with a value of session and changeSession.
 */
const AuthProvider = ({ initialValueSession, children }) => {
  const [session, setSession] = useState(initialValueSession);
 /**
  * ChangeSession is a function that takes in a newSession and sets the session to that newSession.
  */
  const changeSession = (newSession) => {
    setSession(newSession);
  };
  return <Provider value={{ session, changeSession }}>{children}</Provider>;
};

export { AuthProvider, Consumer as AuthConsumer, AuthContext, sessionInfo };
