from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import *
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password


# This is a serializer for the User model that requires a username, and a password that is at least 8
# characters long and is write-only.
class UserSerializer(ModelSerializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(
        min_length=8, write_only=True
    )

    def validate_password(self, value):
        """
        It takes a string as input, and returns a string that is the hashed version of the input
        
        :param value: The value of the field being validated
        :return: The value of the password is being hashed and salted.
        """
        return make_password(value)

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name',
                  'email', 'password')


class MedicoSerializer(ModelSerializer):
    class Meta:
        model = Medico
        fields = [
            'id',
            'nombre',
            'apellido',
            'dni',
            'direccion',
            'correo',
            'telefono',
            'sexo',
            'num_colegiatura',
            'fecha_nacimiento',
        ]


class EspecialidadSerializer(ModelSerializer):
    class Meta:
        model = Especialidad
        fields = [
            'id',
            'nombre',
            'description',
        ]


class MedicosEspecialidadesSerializer(ModelSerializer):
    class Meta:
        model = MedicosEspecialidades
        fields = [
            'id',
            'medico_id',
            'especialidad',
        ]


class HorariosSerializer(ModelSerializer):
    medico_set = MedicoSerializer(many=True, read_only=True)

    class Meta:
        model = Horarios
        fields = [
            'id',
            'medico_id',
            'activo',
            'fecha_registro',
            'usuario_registro',
            'fecha_modificacion',
            'usuario_modificacion',
            'fecha_atencion',
            'inicio_atencion',
            'fin_atencion',
            'medico_set',
        ]


class PacienteSerializer(ModelSerializer):
    class Meta:
        model = Paciente
        fields = [
            'id',
            'nombres',
            'apellidos',
            'dni',
            'direccion',
            'telefono',
            'sexo',
            'fecha_nacimiento',
        ]


class CitasSerializer(ModelSerializer):
    class Meta:
        model = Citas
        fields = [
            'id',
            'medico_id',
            'paciente_id',
            'estado',
        ]
