from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *


router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'paciente', PacienteViewSet)
router.register(r'especialidad', EspecialidadViewSet)
router.register(r'medico', MedicoViewSet)
router.register(r'medicoEspecialidad', MedicosEspecialidadesViewSet)
router.register(r'horario', HorariosViewSet)
router.register(r'cita', CitasViewSet)


urlpatterns = [
    path('auth/login/', LoginView.as_view(), name='auth-login'),
    path('auth/logout/', LogoutView.as_view(), name='auth-logout'),
    path('auth/signup/', SignupView.as_view(), name='auth-signup'),
] + router.urls
