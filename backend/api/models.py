from django.db import models
from django.contrib.auth.models import AbstractUser


options = (
    ('M', "Masculino"),
    ('F', "Femenino")
)

class User(AbstractUser):
    pass


class Usuario(models.Model):
    activo = models.BooleanField(default=True)
    fecha_registro = models.DateField(auto_now_add=True)
    usuario_registro = models.DateField(auto_now_add=True)
    fecha_modificacion = models.DateField(auto_now=True)
    usuario_modificacion = models.DateField(auto_now=True)

    class Meta:
        abstract = True


class Atencion(models.Model):
    fecha_atencion = models.DateField()
    inicio_atencion = models.TimeField()
    fin_atencion = models.TimeField()

    class Meta:
        abstract = True


class Medico(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    dni = models.CharField(max_length=8)
    direccion = models.CharField(max_length=50)
    correo = models.EmailField()
    telefono = models.CharField(max_length=15)
    sexo = models.CharField(max_length=1, choices=options)
    num_colegiatura = models.CharField(max_length=10)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return self.nombre + " " + self.apellido


class Especialidad(models.Model):
    nombre = models.CharField(max_length=50)
    description = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre


class MedicosEspecialidades(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE)

    def __str__(self):
        return self.especialidad.nombre


class Horarios(Usuario, Atencion):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE, related_name='medico')

    def __str__(self):
        return self.inicio_atencion + " - " + self.fin_atencion


class Paciente(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    dni = models.CharField(max_length=8)
    direccion = models.CharField(max_length=50)
    telefono = models.CharField(max_length=15)
    sexo = models.CharField(max_length=1, choices=options)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return self.nombres + " " + self.apellidos


class Citas(models.Model):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente_id = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.medico_id.nombre + ' ' + self.medico_id.apellido + ' ' + self.paciente_id.nombres + ' ' + self.paciente_id.apellidos
