from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .models import *


admin.site.register(Medico)
admin.site.register(Especialidad)
admin.site.register(MedicosEspecialidades)
admin.site.register(Horarios)
admin.site.register(Paciente)
admin.site.register(Citas)

@admin.register(get_user_model())
class UserAdmin(UserAdmin):
    pass