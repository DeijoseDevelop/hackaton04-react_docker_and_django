from django.test import TestCase
from rest_framework import status
from .models import User
from rest_framework.test import APIClient, RequestsClient, APITestCase

url = 'http://localhost:8000/api'

class AccountTests(APITestCase):
    def test_create_account(self):
        data = {'username': "deijose", 'password': "deijose1230"}
        response = self.client.post(f'{url}/auth/signup/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get(username='deijose').username, 'deijose')

    def test_login_account(self):
        user = User(username='deijose')
        user.set_password('deijose1230')
        user.save()
        data = {"username": "deijose", "password": "deijose1230"}
        response = self.client.post(f'{url}/auth/login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_logout_account(self):
        user = User(username='deijose')
        user.set_password('deijose1230')
        user.save()
        response = self.client.post(f'{url}/auth/logout/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

